﻿using Commmon;
using CSharpFunctionalExtensions;
using System.Collections.Generic;
using System.Linq;

namespace Repository
{
    public class PersonRepository
    {

        private readonly List<Person> people = new List<Person>
        {
            new Person(PersonName.Create("Charith").Value,Email.Create("cmadu701@gmail.com").Value),
            new Person(PersonName.Create("Charith2").Value,Email.Create("cmadu702@gmail.com").Value),
            new Person(PersonName.Create("Charith3").Value,Email.Create("cmadu703@gmail.com").Value)
        };


        //this method is signature honest method.
        //this is called million doller problem in programming.
        //this method is more readable.
        public Maybe<Person> GetPerson(string name)
        {
            Person person = people.Where(p => p.Name == name).FirstOrDefault();

            return Maybe<Person>.From(person);
        }

    }
}
