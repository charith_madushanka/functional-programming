﻿using Commmon;
using CSharpFunctionalExtensions;
using Repository;
using System.Collections.Generic;

namespace Service
{
    public class PersonService
    {

        private readonly List<Person> people = new List<Person>
        {
            new Person(PersonName.Create("Charith Madushanka").Value,Email.Create("cmadu701@gmail.com").Value),
            new Person(PersonName.Create("Charith Madushanka2").Value,Email.Create("cmadu702@gmail.com").Value),
            new Person(PersonName.Create("Charith Madushanka3").Value,Email.Create("cmadu703@gmail.com").Value)
        };


        //in functional programming, exception will be thrown in a exception
        //if only if application bug
        //exception will not be thrown for if we know the result to be
        public Result<Person> GetPersonResult(string name)
        {
            PersonRepository personRepository = new PersonRepository();

            Maybe<Person> personOrNothing = personRepository.GetPerson(name);
            if (personOrNothing.HasNoValue)
            {
                return Result.Fail<Person>($"Person Cannot be found for name {name}");
            }

            return Result.Ok(personOrNothing.Value);
        }

    }
}
