﻿using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace FunctionlProgramming.Controllers
{
    [RoutePrefix("api/people")]
    public class PeopleController : ApiController
    {
        // GET: api/People
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }


        [Route("{name}")]
        [HttpGet]
        public IHttpActionResult GetPerson(string name)
        {
            PersonService personService = new PersonService();

            var result = personService.GetPersonResult(name);
            if (result.IsFailure) {

                return BadRequest(result.Error);
            }


            return Ok(new
            {
                Email = (string)result.Value.Email,
                Name = (string)result.Value.Name
            });
        }

        // POST: api/People
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/People/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/People/5
        public void Delete(int id)
        {
        }
    }
}
