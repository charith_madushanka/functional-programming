﻿using System;

namespace Commmon
{
    public class Person
    {
        public PersonName Name { get; private set; }
        public Email Email { get; private set; }

        //avoid primitive obsession
        public Person(PersonName name, Email email)
        {
            Name = name ?? throw new ArgumentNullException("name");
            Email = email ?? throw new ArgumentNullException("email");
        }

        public void ChangeName(PersonName name)
        {

            Name = name ?? throw new ArgumentNullException(nameof(name));
        }

        public void ChangeEmail(Email email)
        {
            Email = email ?? throw new ArgumentNullException(nameof(email));
        }
    }

}
