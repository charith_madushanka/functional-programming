﻿using CSharpFunctionalExtensions;

namespace Commmon
{
    public class PersonName
    {
        private readonly string value;

        private PersonName(string value)
        {
            this.value = value;
        }

        public static Result<PersonName> Create(string name)
        {
            if (string.IsNullOrWhiteSpace(name))
                return Result.Fail<PersonName>("Name can't be empty");

            if (name.Length > 50)
                return Result.Fail<PersonName>("Name is too long");

            return Result.Ok(new PersonName(name));
        }

        public static implicit operator string(PersonName name)
        {
            return name.value;
        }

        public override bool Equals(object obj)
        {
            PersonName name = obj as PersonName;

            if (ReferenceEquals(name, null))
                return false;

            return value == name.value;
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }
    }
}
